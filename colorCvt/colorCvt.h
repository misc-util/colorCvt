// rgb_hsv.h
//
// Part of colorCvt
//
// Contains fonctions to convert HSV to RGB.
//
// (c) 2017 Sébastien Garmier

#include <algorithm>
#include <cstdint>
#include <cmath>

#ifndef RGB_HSV_H
#define RGB_HSV_H

namespace ColorCvt {

/// \brief Stores RGB color data
///
/// Components are always in the range [0, 1].
///
/// \tparam Color_T Floating point type used for the components.
template<typename Color_T = float>
struct RGB {
	// Assert the types are either double or float
	static_assert(std::is_floating_point<Color_T>::value, "Color_T must be a floating point type");

	/// \brief red
	Color_T r;
	/// \brief green
	Color_T g;
	/// \brief blue
	Color_T b;
};

/// \brief Stores HSV color data
///
/// Hue is in the range [0, 360].
/// Saturation and value are in the range [0, 1].
///
/// \tparam Color_T Floating point type used for the components.
template<typename Color_T = float>
struct HSV {
	// Assert the types are either double or float
	static_assert(std::is_floating_point<Color_T>::value, "Color_T must be a floating point type");

	/// \brief hue
	Color_T h;
	/// \brief saturation
	Color_T s;
	/// \brief value
	Color_T v;
};

/// \brief Converts RGB to HSV.
/// \tparam Color_T Floating point type used for the components.
/// \return HSV color value.
/// \param rgb RGB color value.
template<typename Color_T = float>
HSV<Color_T> RGBtoHSV(RGB<Color_T> const& rgb) {
	// Assert the types are either double or float
	static_assert(std::is_floating_point<Color_T>::value, "Color_T must be a floating point type");

	HSV<Color_T> hsv;

	// Constants
	const static Color_T HUE_ANGLE = Color_T(60);

	// Min and max components
	uint32_t minComponent = 0;
	uint32_t maxComponent = 0;
	Color_T min = rgb.r;
	Color_T max = rgb.r;

	if(rgb.g > max) { max = rgb.g; maxComponent = 1; }
	if(rgb.b > max) { max = rgb.b; maxComponent = 2; }

	if(rgb.g < min) { min = rgb.g; minComponent = 1; }
	if(rgb.b < min) { min = rgb.b; minComponent = 2; }

	Color_T delta = max - min;

	// Hue
	if(delta == Color_T(0)) {
		hsv.h = Color_T(0);
	} else {
		switch(maxComponent) {
			case 0: // red
				hsv.h = HUE_ANGLE * fmod((rgb.g - rgb.b) / delta, Color_T(6));
				break;
			case 1: // green
				hsv.h = HUE_ANGLE * ((rgb.b - rgb.r) / delta + Color_T(2));
				break;
			case 2: // blue
				hsv.h = HUE_ANGLE * ((rgb.r - rgb.g) / delta + Color_T(4));
				break;
		}
	}

	// Saturation
	if(max == Color_T(0)) {
		hsv.s = Color_T(0);
	} else {
		hsv.s = delta / max;
	}

	// Value
	hsv.v = max;

	// Return
	return hsv;
}

/// \brief Converts HSV to RGB.
/// \tparam Color_T Floating point type used for the components.
/// \return RGB color value.
/// \param rgb HSV color value.
template<typename Color_T>
RGB<Color_T> HSVtoRGB(HSV<Color_T> const& hsv) {
	// Assert the types are either double or float
	static_assert(std::is_floating_point<Color_T>::value, "Color_T must be a floating point type");

	RGB<Color_T> rgb;

	// Constants
	const static Color_T HUE_ANGLE = Color_T(60);

	Color_T c = hsv.v * hsv.s;
	Color_T x = c * (Color_T(1) - std::fabs(std::fmod(hsv.h / HUE_ANGLE, Color_T(2)) - Color_T(1)));
	Color_T m = hsv.v - c;

	// Components
	if(hsv.h < Color_T(60)) {
		rgb.r = c;
		rgb.g = x;
		rgb.b = Color_T(0);
	} else if(hsv.h < Color_T(120)) {
		rgb.r = x;
		rgb.g = c;
		rgb.b = Color_T(0);
	} else if(hsv.h < Color_T(180)) {
		rgb.r = Color_T(0);
		rgb.g = c;
		rgb.b = x;
	} else if(hsv.h < Color_T(240)) {
		rgb.r = Color_T(0);
		rgb.g = x;
		rgb.b = c;
	} else if(hsv.h < Color_T(300)) {
		rgb.r = x;
		rgb.g = Color_T(0);
		rgb.b = c;
	} else if(hsv.h < Color_T(360)) {
		rgb.r = c;
		rgb.g = Color_T(0);
		rgb.b = x;
	}

	rgb.r += m;
	rgb.g += m;
	rgb.b += m;

	// Return
	return rgb;
}

}

#endif
